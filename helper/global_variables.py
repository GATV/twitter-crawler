import os 
import warnings


# global  
# Ports and Hosts
# Add environment variables
host: str = os.getenv("HOST_PORT") if "HOST_PORT" in os.environ else "0.0.0.0"
port: int = int(os.getenv("API_PORT")) if "API_PORT" in os.environ else 5003

