FROM python:3.6
COPY . /app
WORKDIR /app
RUN apt-get update
RUN pip install -r requirements.txt
RUN python -m nltk.downloader vader_lexicon
#CMD ["python", "start_streaming_processor.py"]
CMD ["python", "run.py"]

