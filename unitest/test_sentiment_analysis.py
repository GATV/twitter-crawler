from analysis.sentiment_analysis import SentimentAnalysisAPI

# 1. Get the language of the document
doc: str = """This is how you get over Corona boredom. “Indoor Happy Gilmore Swings” 😂⛳️ Never a dull moment with my man Coop! jessicalynnsharrock @ Roanoke, Texas https://t.co/Y7tnwBxi2Z"""

res_flair = SentimentAnalysisAPI.get_flair_sentiment_analysis(
    doc=doc)
res_textblob = SentimentAnalysisAPI.get_textblob_sentiment_analysis(
    doc=doc)
res_nltk = SentimentAnalysisAPI.get_nltk_sentiment_analysis(
    doc=doc)