import pandas as pd
import os

local_storage: str = "D:\\DAVID\\Datasets\\Climate_change_tweets"

for i in range(1, 4):
    print(i)
    filename: str = f"climate_id_0{i}.txt"
    filepath: str = os.path.join(local_storage, filename)
    data = pd.read_csv(filepath, sep=" ", header=None)

    data.to_csv(filepath.replace(".txt", ".csv"), index=False, header=False)
