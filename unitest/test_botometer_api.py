import os
from analysis.bot_analysis import BotAnalysisAPI
from helper.settings import consumer_key, consumer_secret, access_token, access_token_secret, botometer_api_key

os.environ["TW_CONSUMER_KEY"] = consumer_key
os.environ["TW_CONSUMER_SECRET"] = consumer_secret
os.environ["TW_ACCESS_TOKEN"] = access_token
os.environ["TW_ACCESS_TOKEN_SECRET"] = access_token_secret
os.environ["BOTOMETER_API_KEY"] = botometer_api_key


bot_an_api = BotAnalysisAPI()

user_id: int = 1548959833
res = bot_an_api.analyse_account_by_id(user_id=user_id)




