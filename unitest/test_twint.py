import twint
from twint import output, Config
from data_models.twitter_models import StatusTwintDoc, UserAccountDoc
from helper.settings import logger


# Configure

c: Config = twint.Config()
c.Hide_output = True
c.Store_object = True
c.Profile_full = True
c.Limit = 1
c.User_id = "1967210857"
twint.run.Lookup(c)

tweets: list = twint.output.tweets_list

import twint

conf = twint.Config()
conf.Username = ""
conf.Limit = 1
conf.Store_object = True
conf.User_full = True
twint.run.Lookup(conf)
accounts = twint.output.users_list


def execute_twint_streaming(config: Config, limit: int = 100):
    try:
        config.Limit: int = limit
        while True:
            # 1. Process searching
            twint.run.Search(config)

            # 2. Retrieve data
            tweets: list = twint.output.tweets_list
            users: list = twint.output.users_list
            # 3. Process Status + User
            for tweet in tweets:
                status_doc: StatusTwintDoc = StatusTwintDoc(status=tweet)

                # Get user from user id

                user: StatusTwintDoc = UserAccountDoc(user=user)

    except Exception as e:
        logger.error(e)

