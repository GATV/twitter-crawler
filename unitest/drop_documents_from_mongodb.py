import pymongo

db_name = "COVID_19"
collection_names = ["tweets", "users"]

myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient[db_name]

for collection in collection_names:
    mycol = mydb[collection]
    x = mycol.delete_many({})