
angular
	.module("fandango")
	.controller("main",['$scope','$http','Api','$location',function($scope,$http,Api,$location){

		$scope.query=""
		$scope.hashtags={ 
										trump: ["id",true,150],
										Covid19: ["id",true ,1043],
										AstraZeneca: ["id",false ,831]
									};
		$scope.searching=false;
		$scope.keyPress = function(keyEvent) {
			if (keyEvent.which === 13){
				$scope.add($scope.query);
			}
		}
		var getHashtags= function(){
			Api.getHashtags(
						function(response) {
							console.log(response)
							$scope.hashtags=response.data.hashtags;
							$scope.status=response.data.status;
						},	
						function (error){
							$scope.hashtags=[];
							$scope.status=[];

						}
					)
		}
		$scope.add= function(hashtag){
			Api.start(hashtag,
						function(response) {
							$scope.hashtags=response.data.hashtags;
							$scope.status=response.data.status;
							$scope.query=""
						},	
						function (error){
							
						}
					)
		}
		
		$scope.stop= function(hashtag){
			Api.pause(hashtag,
						function(response) {
							$scope.hashtags=response.data.hashtags;
							$scope.status=response.data.status;
							$scope.query=""
						},	
						function (error){
							
						}
					)
		}
		getHashtags();

	}])
	