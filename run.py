import json
import os
from flask import Flask, request
from flask_cors import CORS,cross_origin

#from services.services import GraphAnalysisService
#from helper import global_variables as gv
from helper import global_variables as gv

#from twitter_processors.streaming_processor import StreamingProcessor
#from connectors.twitter_api_connector import TwitterConnector
#from helper.settings import consumer_key, consumer_secret, access_token, access_token_secret


app = Flask(__name__,static_folder=os.path.join(os.getcwd(), 'www'))
CORS(app)


hashtags =[]
status =[]

@app.route('/', methods=['GET'])
@cross_origin()
def root():
    return app.send_static_file('index.html')

# GET to extract the hasthags
@app.route('/hashtags', methods=['GET'])
def search():
    return json.dumps({"hashtags":hashtags,"status":status})

@app.route('/start/<hashtag>', methods=['GET'])
def startHashtag(hashtag):
    if  hashtag in hashtags:
         status[hashtags.index(hashtag)]=True
    else:
        hashtags.append(hashtag.lower())
        status.append(True)
        #llamarDavid(hashtag,True)
    return json.dumps({"hashtags":hashtags,"status":status})

@app.route('/pause/<hashtag>', methods=['GET'])
def pauseHashtag(hashtag):
    if  hashtag in hashtags:
        status[hashtags.index(hashtag)]=False
        #llamarDavid(hashtag,False)
    return json.dumps({"hashtags":hashtags,"status":status})

# def llamarDavid(hasthag,status):
#     if status == True: 
#         # Set up the object
#         twitter_connector: TwitterConnector = TwitterConnector(
#             consumer_key=consumer_key, consumer_secret=consumer_secret,
#             access_token=access_token, access_token_secret=access_token_secret)

#         # Twitter API Connection
#         twitter_connector.set_up_twitter_api_connection()

#         # Processing data
#         streaming_processor: StreamingProcessor = StreamingProcessor(
#             api=twitter_connector.api,
#             languages=["en"],
#             track=[hasthag],
#             db_name=db_name,
#             collection_names=["test_collection"],
#             storage=["elasticsearch"],
#             add_sentiment=False,
#             add_bot_analysis=False)

#         streaming_processor.run_twitter_streaming()
#     else:
#         print("killing the process")



if __name__ == '__main__':
    app.run(debug=False, host=gv.host, port=gv.port, threaded=True)