
PATH="http://localhost:5003/";

angular
	.module("fandango")
	.factory("Api" ,function($http){
		var factory = {};
		var apiKey =  null;
		factory.getHashtags = function(callback,error) {
			var url=PATH+"hashtags";
			 let req = {
                        url: PATH+'hashtags',
                        method: 'GET',
                       
                    }
              return  $http(req).then(callback,error);
			 
			
		}
		factory.start = function(hashtag,callback,error) {
			 let req = {
                        url: PATH+'start/'+hashtag,
                        method: 'GET',
                       
                    }
              return  $http(req).then(callback,error);
			
		}
		factory.pause = function(hashtag,callback,error) {
			 let req = {
                        url: PATH+'pause/'+hashtag,
                        method: 'GET',
                       
                    }
              return  $http(req).then(callback,error);
			
		}

		return factory;
	})
