from api.api import TWStreamingAPI

twst_api: TWStreamingAPI = TWStreamingAPI()

thread_name: str = "New streaming thread"
db_name: str = "test_twitter_v5"
collection_name: str = "test_collection"
data = {"languages": ["fr", "en"], "track": ["COVID"],
        "storage": "elasticsearch",
        "collection_names": {"status": f"tweets",
                             "user": "users"},
        "mongo_db_name": db_name}
twst_api.start_new_streaming_process(
    thread_name=thread_name, data=data)
